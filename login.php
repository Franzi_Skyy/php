<?php
  require_once('dbvar.php');

  // Sitzung starten
  session_start();

  // Fehlermeldung zurücksetzen
  $errormsg = "";

  // Versuchen, den Benutzer einzuloggen, wenn er nicht eingeloggt ist
    if (!isset($_SESSION['user_id'])) {
    if (isset($_POST['submit'])) {
      // Mit databank verbinden
      $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
      mysqli_set_charset($db, "utf8"); 
      
      // Die eingegebenen Login-data abrufen
      $auth_username = mysqli_real_escape_string($db, trim($_POST['username']));
      $auth_password = mysqli_real_escape_string($db, trim($_POST['password']));

      if (!empty($auth_username) && !empty($auth_password)) {
        // username und password in der Tabelle nachschlagen
        $sql = "SELECT user_id, username FROM lg_user WHERE username = '$auth_username' AND " .
          "password = SHA('$auth_password')";
        $data = mysqli_query($db, $sql);

        if (mysqli_num_rows($data) == 1) {
          // Login erfolgreich, also die Cookies setzen und den Benutzer zur homepage umleiten
          $row = mysqli_fetch_array($data);
    	    $_SESSION['user_id'] = $row['user_id'];
          $_SESSION['username'] = $row['username'];
          setcookie('user_id', $row['user_id'], time() + (60 * 60 * 24 * 30));    // Verfällt in 30 Tagen
          setcookie('username', $row['username'], time() + (60 * 60 * 24 * 30));  // Verfällt in 30 Tagen
          $homepage = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
          header('Location: ' . $homepage);
        }
        else {
          // Beusername/password falsch, also Fehlermeldung setzen
          $errormsg = 'Incorrect login data.';
        }
      }
      else {
        // Beusername/password nicht eingegeben, also Fehlermeldung setzen
        $errormsg = 'Login data is required.';
      }
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lovegenerator - Login</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
  <h3>Lovegenerator - Login</h3>

<?php
  // Wenn Sitzungsvariablen leer sind, Fehlermeldungen und Login-Formular anzeigen; andernfalls Login bestätigen
  if (empty($_SESSION['user_id'])) {
  	echo '<p class="fehler">' . $errormsg . '</p>';
?>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <fieldset>
      <legend>Login</legend>
      <label for="username">Username:</label>
      <input type="text" id="username" name="username"
        value="<?php if (!empty($auth_username)) echo $auth_username; ?>" /><br />
      <label for="password">Password:</label>
      <input type="password" id="password" name="password" />
    </fieldset>
    <input type="submit" value="Login" name="submit" />
  </form>
 	
<?php
  }
  else {
    // Login bestätigen
    echo('<p class="login">Welcome, ' . $_SESSION['username'] . '.</p>');
  }
?>
</body>
</html>
