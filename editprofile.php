<?php
  session_start();

  // Wenn Sitzungsvariablen nicht gesetzt sind, versuchen sie über Cookies zu setzen
    if (!isset($_SESSION['user_id'])) {
    if (isset($_COOKIE['user_id']) && isset($_COOKIE['username'])) {
      $_SESSION['user_id'] = $_COOKIE['user_id'];
      $_SESSION['username'] = $_COOKIE['username'];
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lovegenerator - Edit Profile</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
  <h3>Lovegenerator - Edit Profile</h3>

<?php
  require_once('picvar.php');
  require_once('dbvar.php');

  // Vor weiteren Schritten prüfen, ob der Benutzer eingeloggt ist
  if (!isset($_SESSION['user_id'])) {
    echo '<p class="login">To use this site you need to <a href="login.php">log in</a>.</p>';
    exit();
  }
  else {
    echo('<p class="login">Welcome, ' . $_SESSION['username'] . '. <a href="logout.php">Logout</a>.</p>');
  }

  // Mit Datenbank verbinden
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($db, "utf8"); 
  
  if (isset($_POST['submit'])) {
    // Profildaten aus $_POST auslesen
    $first_name = mysqli_real_escape_string($db, trim($_POST['first_name']));
    $last_name = mysqli_real_escape_string($db, trim($_POST['last_name']));
    $gender = mysqli_real_escape_string($db, trim($_POST['gender']));
    $birthday = mysqli_real_escape_string($db, trim($_POST['birthday$birthday']));
    $city = mysqli_real_escape_string($db, trim($_POST['city']));
    $old_pic = mysqli_real_escape_string($db, trim($_POST['old_pic']));
    $new_pic = mysqli_real_escape_string($db, trim($_FILES['new_pic']['name']));
    $new_pic_type = $_FILES['new_pic']['type'];
    $new_pic_size = $_FILES['new_pic']['size']; 
    list($new_pic_width, $new_pic_height) = getimagesize($_FILES['new_pic']['tmp_name']);
    $fail = false;

    // Falls erforderlich hochgeladene picdatei prüfen und verschieben
    if (!empty($new_pic)) {
      if ((($new_pic_type == 'image/gif') || ($new_pic_type == 'image/jpeg') || ($new_pic_type == 'image/pjpeg') ||
        ($new_pic_type == 'image/png')) && ($new_pic_size > 0) && ($new_pic_size <= LG_MAXDATEIGROESSE) &&
        ($new_pic_width <= LG_MAXBILDBREITE) && ($new_pic_height <= LG_MAXBILDHOEHE)) {
        if ($_FILES['file']['error'] == 0) {
          // Die hochgeladene Datei in das picverzeichnis verschieben
          $track = LG_IMAGESPFAD . basename($new_pic);
          if (move_uploaded_file($_FILES['new_pic']['tmp_name'], $track)) {
            // Neue picdatei wurde erfolgreich hochgeladen, also alte löschen
            if (!empty($old_pic) && ($old_pic != $new_pic)) {
              @unlink(LG_IMAGESPFAD . $old_pic);
            }
          }
          else {
            // Neues pic konnte nicht verschoben werden, also Datei löschen und Fehler-Flag setzen
            @unlink($_FILES['new_pic']['tmp_name']);
            $fail = true;
            echo '<p class="fail">Something went wrong. Upload is not possible</p>';
          }
        }
      }
      else {
        // Die neue picdatei ist nicht gültig, also Datei löschen und Fehler-Flag setzen
        @unlink($_FILES['new_pic']['tmp_name']);
        $fail = true;
        echo '<p class="fail">This file has to be a GIF-, JPEG- or PNG-file and shouldnt be' .
          '  bigger than ' . (LG_MAXDATEIGROESSE / 1024) . ' KB or ' . LG_MAXBILDBREITE . 'x' . LG_MAXBILDHOEHE . ' Pixel.</p>';
      }
    } 

    // Die Profildaten in der Datenbank aktualisieren
    if (!$fail) {
      if (!empty($first_name) && !empty($last_name) && !empty($gender) && !empty($birthday) && !empty($city) ) {
        // Bildspalte nur setzen, wenn es ein neues Bild gibt
        if (!empty($new_pic)) {
          $sql = "UPDATE lg_user SET first_name = '$first_name', last_name = '$last_name', gender = '$gender', " .
            " birthday$birthday = '$birthday', city = '$city', pic = '$new_pic' WHERE user_id = '" . $_SESSION['user_id'] . "'";
        }
        else {
          $sql = "UPDATE lg_user SET first_name = '$first_name', last_name = '$last_name', gender = '$gender', " .
            " birthday$birthday = '$birthday', city = '$city' WHERE user_id = '" . $_SESSION['user_id'] . "'";
        }
        mysqli_query($db, $sql);

        // Aktualisierung des Profils bestätigen
        echo '<p>Your profile is updated. You wanna see the new version <a href="showprofile.php">Show profile</a>?</p>';

        mysqli_close($db);
        exit();
      }
      else {
        echo '<p class="fail">You have to enter the required data (pic is optional).</p>';
      }
    }
  } // Ende Prüfung auf übermitteltes Formular
  else {
    // Profil aus der Datenbank abrufen
    $sql = "SELECT first_name, last_name, gender, birthday$birthday, city, pic FROM lg_user WHERE user_id = '" . $_COOKIE['user_id'] . "'";
    $daten = mysqli_query($db, $sql);
    $row = mysqli_fetch_array($daten);

    if ($row != NULL) {
      $first_name = $row['first_name'];
      $last_name = $row['last_name'];
      $gender = $row['gender'];
      $birthday = $row['birthday$birthday'];
      $city = $row['city'];
      $old_pic = $row['pic'];
    }
    else {
      echo '<p class="fail">We couldnt access your profile.</p>';
    }
  }

  mysqli_close($db);
?>

  <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo LG_MAXDATEIGROESSE; ?>" />
    <fieldset>
      <legend>Personal data</legend>
      <label for="first_name">First Name:</label>
      <input type="text" id="first_name" name="first_name" value="<?php if (!empty($first_name)) echo $first_name; ?>" /><br />
      <label for="last_name">Last Name:</label>
      <input type="text" id="last_name" name="last_name" value="<?php if (!empty($last_name)) echo $last_name; ?>" /><br />
      <label for="gender">Gender:</label>
      <select id="gender" name="gender">
        <option value="M" <?php if (!empty($gender) && $gender == 'M') echo 'selected = "selected"'; ?>>Man</option>
        <option value="W" <?php if (!empty($gender) && $gender == 'W') echo 'selected = "selected"'; ?>>Woman</option>
      </select><br />
      <label for="birthday$birthday">Birthday:</label>
      <input type="text" id="birthday$birthday" name="birthday$birthday" value="<?php if (!empty($birthday)) echo $birthday; else echo 'JJJJ-MM-TT'; ?>" /><br />
      <label for="city">City:</label>
      <input type="text" id="city" name="city" value="<?php if (!empty($city)) echo $city; ?>" /><br />
      <input type="hidden" name="old_pic" value="<?php if (!empty($old_pic)) echo $old_pic; ?>" />
      <label for="new_pic">Picture:</label>
      <input type="file" id="new_pic" name="new_pic" />
      <?php if (!empty($old_pic)) {
        echo '<img class="profil" src="' . LG_IMAGESPFAD . $old_pic . '" alt="Profilpic" />';
      } ?>
    </fieldset>
    <input type="submit" value="Save profile" name="submit" />
  </form>
</body> 
</html>
