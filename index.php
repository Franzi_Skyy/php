<?php
  session_start();

  // Wenn Sitzungsvariablen nich gesetzt sind, versuchen sie über Cookies zu setzen
  if (!isset($_SESSION['user_id'])) {
    if (isset($_COOKIE['user_id']) && isset($_COOKIE['username'])) {
      $_SESSION['user_id'] = $_COOKIE['user_id'];
      $_SESSION['username'] = $_COOKIE['username'];
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lovegenerator</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
  <h3>Lovegenerator</h3>

<?php
  require_once('picvar.php');
  require_once('dbvar.php');

  // Navigationsmenü generieren
  if (isset($_SESSION['username'])) {
	  echo '&#10084; <a href="showprofile.php">Show profile</a><br />';
	  echo '&#10084; <a href="editprofile.php">Edit profile</a><br />';
	  echo '&#10084; <a href="logout.php">Logout (' . $_SESSION['username'] . ')</a>';
	}
	else {
	  echo '&#10084; <a href="login.php">Log In</a><br />';
	  echo '&#10084; <a href="signup.php">Sign Up</a>';
	}
  

  // Mit Datenbank verbinden 
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($db, "utf8");
  

  // Benutzerdaten aus Datenbank abrufen
  $sql = "SELECT user_id, first_name, pic FROM lg_user WHERE first_name IS NOT NULL ORDER BY registerdate DESC LIMIT 5";
  $daten = mysqli_query($db, $sql);

  // Das Array mit den Benutzerdaten durchlaufen und Daten mit HTML formatieren
  echo '<h4>New member:</h4>';
  echo '<table>';
	while ($zeile = mysqli_fetch_array($daten)) {
    if (is_file(LG_IMAGESPFAD . $zeile['pic']) && filesize(LG_IMAGESPFAD . $zeile['pic']) > 0) {
      echo '<tr><td><img src="' . LG_IMAGESPFAD . $zeile['pic'] . '" alt="' . $zeile['first_name'] . '" /></td>';
    }
    else {
      echo '<tr><td><img src="' . LG_IMAGESPFAD . 'nopic.jpg' . '" alt="' . $zeile['first_name'] . '" /></td>';
    }
    if (isset($_SESSION['user_id'])) {
      echo '<td><a href="showprofile.php?user_id=' . $zeile['user_id'] . '">' . $zeile['first_name'] . '</a></td></tr>';
    }
    else {
      echo '<td>' . $zeile['first_name'] . '</td></tr>';
    }
  }
  echo '</table>';

  mysqli_close($db);
?>

</body> 
</html>
