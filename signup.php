<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lovegenerator - Sign Up</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
  <h3>Lovegenerator - Sign Up</h3>

<?php
  require_once('appvars.php');
  require_once('verbvars.php');

  // Datenbank verbinden
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($db, "utf8"); 
  
  if (isset($_POST['submit'])) {
    // Anmeldedaten aus $_POST auslesen
    $username = mysqli_real_escape_string($db, trim($_POST['username']));
    $password1 = mysqli_real_escape_string($db, trim($_POST['password1']));
    $password2 = mysqli_real_escape_string($db, trim($_POST['password2']));

    if (!empty($username) && !empty($password1) && !empty($password2) && ($password1 == $password2)) {
      // Check if name is available

      $sql = "SELECT * FROM lg_user WHERE username = '$username'";
      $data = mysqli_query($db, $sql);
      if (mysqli_num_rows($data) == 0) {
        // Checked user, send data to db
        $sql = "INSERT INTO lg_user (username, passwort, registerdate) VALUES ('$username', SHA('$password1'), NOW())";
        mysqli_query($db, $sql);

        // Login successful
        echo '<p>Your Account is created. Now you can log in and ' .
          '<a href="editprofile.php">work on your profile.</a>.</p>';

        mysqli_close($db);
        exit();
      }      else {
        // username is already used
        echo '<p class="error">This username is already used. ' .
          'Please choose another one.</p>';
        $username = "";
      }
    }
    else {
      echo '<p class="error">Please enter the required data. Dont forget ' .
        'to repeat the password.</p>';
    }
  }

  mysqli_close($db);
?>

<p>Choose your username and a password for your new account.</p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <fieldset>
    <legend>Sign Up Details</legend>
    <label for="username">Username:</label>
      <input type="text" id="username" name="username" value="<?php if (!empty($username)) echo $username; ?>" /><br />
      <label for="password1">Password:</label>
      <input type="password" id="password1" name="password1" /><br />
      <label for="password2">Repeat password:</label>
      <input type="password" id="password2" name="password2" /><br />
    </fieldset>
    <input type="submit" value="Signup" name="submit" />
  </form>
</body> 
</html>
