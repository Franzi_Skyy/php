<?php
  session_start();

  // Wenn Sitzungsvariablen nicht gesetzt sind, versuchen über Cookies zu setzen
    if (!isset($_SESSION['user_id'])) {
    if (isset($_COOKIE['user_id']) && isset($_COOKIE['username'])) {
      $_SESSION['user_id'] = $_COOKIE['user_id'];
      $_SESSION['username'] = $_COOKIE['username'];
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lovegenerator - Profile</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
  <h3>Lovegenerator - Profile</h3>

<?php
  require_once('picvar.php');
  require_once('dbvar.php');

  // Vor weiteren Schritten prüfen, ob der Benutzer eingeloggt ist
  if (!isset($_SESSION['user_id'])) {
    echo '<p class="login">Login to get access to this page <a href="login.php">login</a>.</p>';
    exit();
  }
  else {
    echo('<p class="login">Welcome, ' . $_SESSION['username'] . '. <a href="logout.php">Logout</a>.</p>');
  }
  
  // Mit databank verbinden
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($db, "utf8");
  

  // Profildata aus databank abrufen
  if (!isset($_GET['user_id'])) {
    $sql = "SELECT username, first_name, last_name, gender, birthday, city, pic FROM lg_user WHERE user_id = '" . $_SESSION['user_id'] . "'";
  }
  else {
    $sql = "SELECT username, first_name, last_name, gender, birthday, city, pic FROM lg_user WHERE user_id = '" . $_GET['user_id'] . "'";
  }
  $data = mysqli_query($db, $sql);

  if (mysqli_num_rows($data) == 1) {
    // Benutzerzeile gefunden, also Benutzerdata anzeigen
    $row = mysqli_fetch_array($data);
    echo '<table>';
    if (!empty($row['username'])) {
      echo '<tr><td class="label">Username:</td><td>' . $row['username'] . '</td></tr>';
    }
    if (!empty($row['first_name'])) {
      echo '<tr><td class="label">Firstname:</td><td>' . $row['first_name'] . '</td></tr>';
    }
    if (!empty($row['last_name'])) {
      echo '<tr><td class="label">Lastname:</td><td>' . $row['last_name'] . '</td></tr>';
    }
    if (!empty($row['gender'])) {
      echo '<tr><td class="label">Gender:</td><td>';
      if ($row['gender'] == 'M') {
        echo 'Man';
      }
      else if ($row['Gender'] == 'W') {
        echo 'Woman';
      }
      else {
        echo '?';
      }
      echo '</td></tr>';
    }
    if (!empty($row['birthday'])) {
        if (!isset($_GET['user_id']) || ($_SESSION['user_id'] == $_GET['user_id'])) {
        // Dem Benutzer selbst den Geburtstag anzeigen
        echo '<tr><td class="label">Birthday:</td><td>' . $row['birthday'] . '</td></tr>';
      }
      else {
        // Allen anderen nur das Geburtsjahr anzeigen
        list($year, $month, $day) = explode('-', $row['birthday']);
        echo '<tr><td class="label">Birthday:</td><td>' . $year . '</td></tr>';
      }
    }
    if (!empty($row['city'])) {
      echo '<tr><td class="label">City:</td><td>' . $row['city'] . '</td></tr>';
    }
    if (!empty($row['pic'])) {
      echo '<tr><td class="label">Picture:</td><td><img src="' . LG_IMAGESPFAD . $row['pic'] .
        '" alt="Profilpic" /></td></tr>';
    }
    echo '</table>';
    if (!isset($_GET['user_id']) || ($_SESSION['user_id'] == $_GET['user_id'])) {
      echo '<p>You wanna <a href="editprofile.php">edit profile</a>?</p>';
    }
  } // Ende der Prüfung, auf eine Ergebniszeile
  else {
    echo '<p class="fehler">Error accessing your profile.</p>';
  }

  mysqli_close($db);
    	?>
</body> 
</html>
