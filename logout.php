<?php
  // Wenn Benutzer eingeloggt ist, Sitzungsvariablen löschen und Benutzer ausloggen
  session_start();
  if (isset($_SESSION['user_id'])) {
    // Sitzungsvariablen löschen, indem $_SESSION auf ein leeres Array gesetzt wird
    $_SESSION = array();

    // Sitzungs-Cookie löschen, Verfallsdatum zurückgedrehen
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time() - 3600);
    }

    // Sitzung zerstören
    session_destroy();
  }

  // Cookies löschen, indem wir das Verfallsdatum auf vor eine Stunde (3600 Sekunden) setzen
  setcookie('user_id', '', time() - 3600);
  setcookie('username', '', time() - 3600);

  // Zur Hauptseite zurückleiten
  $homepage = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
  header('Location: ' . $homepage);
?>
