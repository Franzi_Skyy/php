<?php
  // Anwendungskonstanten definieren
  define('LG_IMAGESPFAD', 'images/');
  define('LG_MAXDATEIGROESSE', 32768);      // 32 KB
  define('LG_MAXBILDBREITE', 120);        // 120 Pixel
  define('LG_MAXBILDHOEHE', 120);       // 120 Pixel
?>
