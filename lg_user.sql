CREATE DATABASE IF NOT EXISTS lgdb;
USE lgdb;

--
-- Definition of table `lgdb`.`lg_user`
--

DROP TABLE IF EXISTS `lgdb`.`lg_user`;
CREATE TABLE  `lgdb`.`lg_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `registerdate` datetime default NULL,
  `first_name` varchar(32) character set latin1 default NULL,
  `last_name` varchar(32) character set latin1 default NULL,
  `gender` varchar(1) character set latin1 default NULL,
  `birthday` date default NULL,
  `city` varchar(32) character set latin1 default NULL,
  `pic` varchar(32) character set latin1 default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lgdb`.`lg_user`
--

/*!40000 ALTER TABLE `lg_user` DISABLE KEYS */;
LOCK TABLES `lg_user` WRITE;
INSERT INTO `lgdb`.`lg_user` VALUES
UNLOCK TABLES;
/*!40000 ALTER TABLE `lg_user` ENABLE KEYS */;

